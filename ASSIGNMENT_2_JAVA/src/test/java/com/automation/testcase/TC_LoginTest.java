package com.automation.testcase;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.base.DriverInstance;
import com.automation.pom.LoginPage;
import com.automation.utils.CaptureScreenShot;

public class TC_LoginTest extends DriverInstance {
	
	@Test(dataProvider = "Excel")
	public void TC_001(String email, String password) throws IOException {
	try {
		LoginPage login = new LoginPage(driver);
		login.clickSignInIcon();
		login.enterEmail(email);
		login.enterPassword(password);
		login.clickSignInButton();
		login.checkLoginSuccess();
	} catch (Exception e) {
		System.out.println("Đã xảy ra lỗi: " + e);
		// chụp ảnh màn hình khi xảy ra lỗi và đặt tên ảnh là email sử dụng
		CaptureScreenShot.takeScreenShot(driver, email);
		Assert.fail();
	}
	}
	
	// phương thức lấy data từ file excel
	@DataProvider(name = "Excel")
	public Object [][] testDataGen() throws IOException {
		FileInputStream file = new FileInputStream("./data/assignment2_data_test.xlsx");
		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet loginSheet = workbook.getSheet("Login");
		int numberOfData = loginSheet.getPhysicalNumberOfRows();
		Object [][] testData = new Object [numberOfData][2];
		
		for (int i = 0; i < numberOfData; i++) {
			XSSFRow row = loginSheet.getRow(i);
			XSSFCell username = row.getCell(0);
			XSSFCell password = row.getCell(1);
			testData [i][0] = username.getStringCellValue();
			testData [i][1] = password.getStringCellValue();
		}
		return testData;
	}
}
