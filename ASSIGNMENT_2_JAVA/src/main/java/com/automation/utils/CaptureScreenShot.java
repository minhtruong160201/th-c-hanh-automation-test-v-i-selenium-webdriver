package com.automation.utils;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

public class CaptureScreenShot {
	static String fileName = null;
	
	public static void takeScreenShot(WebDriver driver, String imageName) {
		try {
			BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
			fileName = "./Screenshots/" + imageName + ".png";
			File scs = new File(fileName);
			ImageIO.write(image, "png", scs);
		} catch (Exception e) {
			System.out.println("Xảy ra lỗi khi chụp ảnh màn hình");
			e.printStackTrace();
		}
		attachScreenShotsToReport();
	}
	
	// gắn ảnh chụp màn hình vào report
	public static void attachScreenShotsToReport() {
		try {
			System.setProperty("org.uncommons.reportng.escape-output", "false");
			File f = new File(fileName);
			Reporter.log("<a href=" + f.getAbsolutePath() + ">click to open screenshot</a>");
		} catch (Exception e) {
			System.out.println("Xảy ra lỗi khi đính kèm ảnh màn hình");
		}
	}
}
