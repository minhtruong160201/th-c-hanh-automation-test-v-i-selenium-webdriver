package com.automation.pom;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByXPath;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.automation.utils.PropertiesFileUtils;

public class LoginPage {
	WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	// click icon signin
	public void clickSignInIcon() throws IOException {
		driver.findElement(By.xpath(PropertiesFileUtils.fetchLocatorValue("icon_signin"))).click();
	}
	
	// nhập dữ liệu vào field email
	public void enterEmail(String email) throws IOException {
		driver.findElement(By.xpath(PropertiesFileUtils.fetchLocatorValue("login_email"))).sendKeys(email);
	}
	
	// nhập dữ liệu vào field password
	public void enterPassword(String password) throws IOException {
		driver.findElement(By.xpath(PropertiesFileUtils.fetchLocatorValue("login_password"))).sendKeys(password);
	}
	
	// click icon login
	public void clickSignInButton() throws IOException {
		driver.findElement(By.xpath(PropertiesFileUtils.fetchLocatorValue("login_signin"))).click();
	}
	
	//Kiểm tra đăng nhập thành công hay không
	public void checkLoginSuccess() throws IOException {
		WebElement dangNhapThanhCong = (WebElement) driver.findElement(By.xpath(PropertiesFileUtils.fetchLocatorValue("icon_signout")));
		Assert.assertTrue(dangNhapThanhCong.isDisplayed(), "Đăng nhập không thành công");
	}
}
