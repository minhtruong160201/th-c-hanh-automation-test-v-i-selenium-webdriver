package com.automation.base;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import com.automation.utils.PropertiesFileUtils;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DriverInstance {
	public WebDriver driver;
	
	@BeforeMethod
	public void startDriver() throws IOException {
		// kiếm tra nếu để browser name là chrome thì cài chorme driver
		if(PropertiesFileUtils.fetchProperty("browser_name").toString().equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		// kiếm tra nếu để browser name là firefox thì cài firefox driver
		else if (PropertiesFileUtils.fetchProperty("browser_name").toString().equalsIgnoreCase("fireFox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		// kiếm tra nếu để browser name là ie thì cài ie driver
		else if (PropertiesFileUtils.fetchProperty("browser_name").toString().equalsIgnoreCase("ie")) {
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		}
		// nếu không có browser name thì vẫn cài chorme driver vì chorme là trình duyệt phổ biến nhất
		else {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		// mở đường dẫn trong trình duyệt và phóng to trình duyệt
		driver.get(PropertiesFileUtils.fetchProperty("appURL").toString());
		driver.manage().window().maximize();
	}
	
	// đóng trình duyệt sau
	@AfterMethod
	public void closeDriver() {
		driver.quit();
	}
}
