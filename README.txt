# THỰC HÀNH AUTOMATION TEST VỚI SELENIUM WEBDRIVER
=== Giới thiệu project ===
Project Thực hành automation test với selenium webdriver là script automation test theo 1 kịch bản cho trước
=== Các thành phần trong project ===
- 1 folder ASSIGNMENT_2_JAVA lưu trữ file java của project Thực hành automation test với selenium webdriver
- 1 file word chứa yêu cầu và hưỡng dẫn cách hoàn thành project Thực hành automation test với selenium webdriver
=== Cách cài đặt ===
- Sử dụng phần mềm Eclipse IDE để làm việc với project
- Tại phần mềm Ecplipse IDE vào File -> Open Projects from File System or Archive -> chọn folder ASSIGNMENT_2_JAVA
- Tại phần mềm Eclipse IDE vào Help -> Eclipse Marketplace -> tìm TestNG -> cài đặt -> restart phần mềm Eclipse IDE khi cài đặt xong testNG
=== Cách sử dụng ===
- Tại Package Explorer vào testng.xml -> click button Run as -> click button TestNG Suite
- Sau khi chạy test xong vào test-output -> html -> chuột phải file index.html -> Open With -> Web Browser để đọc kết quả testcase